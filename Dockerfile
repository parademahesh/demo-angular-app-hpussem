# stage 1
FROM node:latest as node
WORKDIR /app
COPY . .
RUN npm cache clean --force
RUN npm install
RUN npm run build --prod
#ENTRYPOINT ["ng", "serve", "-o";]

# stage 2
FROM nginx:alpine
COPY --from=node /app/dist/angular-app /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
